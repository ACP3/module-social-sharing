<div id="tab-share" class="tab-pane fade" data-add-tab-title="{lang t="share|share"}" data-add-tab-identifier="#tab-share">
    {include file="asset:Share/Partials/share_fields.tpl" share=$share}
</div>
{javascripts}
    {include_js module="system" file="partials/add_tab"}
{/javascripts}
